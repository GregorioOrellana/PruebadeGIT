package com.example.truce.evaluacionfragment;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements PrimerFragment.OnFragmentInteractionListener, SegundoFragment.OnFragmentInteractionListener, TercerFragment.OnFragmentInteractionListener {

    PrimerFragment uno;
    SegundoFragment dos;
    TercerFragment tres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uno=new PrimerFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.marco, uno).commit();


    }

    @Override
    public void onFragmentInteraction(String nFragment, String nEvento) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        dos = new SegundoFragment();
        tres = new TercerFragment();

        if(nFragment.equals("BienvenidaFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.marco, dos);



                transaction.commit();
            }
        }

        if(nFragment.equals("dos")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.marco, tres);



                transaction.commit();
            }
        }





    }
}
