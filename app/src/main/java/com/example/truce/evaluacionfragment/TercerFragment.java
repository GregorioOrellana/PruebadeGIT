package com.example.truce.evaluacionfragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TercerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TercerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TercerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View vista3;
    Button BTN3;

    private OnFragmentInteractionListener mListener;

    public TercerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TercerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TercerFragment newInstance(String param1, String param2) {
        TercerFragment fragment = new TercerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        vista3=inflater.inflate(R.layout.fragment_tercer, container, false);

        final Button BTN3 = (Button) vista3.findViewById(R.id.siguiente3);

        BTN3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usuario = ((EditText) vista3.findViewById(R.id.usuario)).getText().toString().trim();
                String contraseña = ((EditText) vista3.findViewById(R.id.contraseña)).getText().toString();

                if(usuario.equals("")){
                    Toast.makeText(BTN3.getContext(), "Compos sin llenar", Toast.LENGTH_SHORT).show();
                }else{

                    if (contraseña.equals("")){
                        Toast.makeText(BTN3.getContext(), "Compos sin llenar", Toast.LENGTH_SHORT).show();
                    }else{

                        Toast.makeText(BTN3.getContext(),"SU CONTRASEÑA ES = "+ contraseña, Toast.LENGTH_LONG).show();
                    }


                }


            }
        });


        return vista3;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("", "");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nombreFragmento, String nombreEvento);
    }
}
